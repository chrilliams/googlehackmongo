require 'smarter_csv'
require 'mongo'

puts "starting import process"

client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'accidents')

# using chunks:
filename = 'incidents.csv'

options = {:chunk_size => 100, :key_mapping => {:unwanted_row => nil, :old_row_name => :new_name}}
n = SmarterCSV.process(filename, options) do |chunk|
      # we're passing a block in, to process each resulting hash / row (block takes array of hashes)
      # when chunking is enabled, there are up to :chunk_size hashes in each chunk
      # MyModel.collection.insert( chunk )   # insert up to 100 records at a time
      # puts chunk.inspect
      client[:accidents].insert_many ( chunk )
end


puts "import process completed"

