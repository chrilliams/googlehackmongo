require 'csv'

namespace :testtasks do
  desc "TODO"
  task import_csv: :environment do
    csv_text = File.read('/home/ubuntu/rails-projects/test/testApp/lib/tasks/incidents_1.csv')
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      Incident.create!(row.to_hash)
    end
  end
end
