class PostcodeController < ApplicationController
  before_filter :validate_params



  def show
  end
  
  def search
  	@postcode=params[:postcode].gsub(/\s+/, "")
  	@postcode_lat_long = @p.getLatLong
	  @incidents = Incident.
      near_sphere(location_array:[@postcode_lat_long["lng"],@postcode_lat_long["lat"]]).
      max_distance(location_array: 1.fdiv(3959)).as_json
 	
   	@incidents_severity = Hash.new(0)
   	@daysofweek = {"Monday" => 0, "Tuesday" => 0, "Wednesday" => 0,"Thursday" => 0,"Friday" => 0,"Saturday" => 0, "Sunday" => 0}
    @incidents_gender = Hash.new(0)
		@incidents_ageband = {'0-5'=>0,'6-10'=>0,'11-15'=>0,'16-20'=>0,'21-25'=>0,'26-35'=>0,'36-45'=>0,'46-55'=>0,'56-65'=>0,'66-75'=>0,'Over 75'=>0,'Data missing'=>0}
 
   	@incidents.each do |i| 
   		severity = view_context.severity_lookup(i)
   		@incidents_severity[severity] = @incidents_severity[severity] + 1

  		daysofweek_lookup_result = view_context.daysofweek_lookup(i)
   		@daysofweek[daysofweek_lookup_result] = @daysofweek[daysofweek_lookup_result] + 1 	

                vehicles = i["vehicles"].as_json
			
                vehicles.each do |v|
                  gender = view_context.gender_lookup(v)
                  @incidents_gender[gender] = @incidents_gender[gender] + 1

                  ageband = view_context.ageband_lookup(v)
                  @incidents_ageband[ageband] = @incidents_ageband[ageband] + 1
                end
   	end	
    
    @authority = @p.local_authority
    if @authority != nil
    	@authority_incidents = Statistic.where( code: @authority.code ).first.try( :value ) || 0
    	@average_incidents = Statistic.avg( :value ).round( 0 )
    
    	@most_incidents = Statistic.where( value: Statistic.max( :value ) ).first
    	@worst_authority = LocalAuth.where( code: @most_incidents.code ).first
  	end
  end

  private
  def validate_params

  	@p = Postcode.new

		if params[:postcode] == ""
			redirect_to root_path, notice: 'Please provide postcode.'
		elsif not /[[:alnum:]]/.match(params[:postcode]) 
			redirect_to root_path, notice: 'Please provide a valid postcode.'
		elsif not @p.is_gb_postcode params[:postcode].gsub(/\s+/, "")
			redirect_to root_path, notice: 'Not a valid UK address'	
  	end
  end
end
