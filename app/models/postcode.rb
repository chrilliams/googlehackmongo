class Postcode

  def is_gb_postcode postcode
    @response ||= perform_lookup postcode
    is_GB=false
    if @response['results'].count > 0
      @response.parsed_response['results'][0]["address_components"].each do |ac|
        if ac["short_name"] == "GB"
          is_GB=true
        end
      end
    end
    is_GB
  end

  def getLatLong
    @response ||= perform_lookup postcode

  	latlong={}
    if @response["results"].count > 0
  	  latlong = @response.parsed_response["results"][0]["geometry"]["location"]
  	end

  	latlong
  end
  
  def local_authority
    auth = nil
    
    @response ||= perform_lookup postcode
    if @response['results'].count > 0
      @response.parsed_response['results'][0]['address_components'].each do |ac|
        auths = LocalAuth.where( name: ac['long_name'])

        auth = auths[0] if auths.count > 0

      end
    end
    
    auth
  end
  
  private 
  
  def perform_lookup postcode
    url = "http://maps.google.com/maps/api/geocode/json?address="+postcode+"&sensor=false"
    @response = HTTParty.get url
  end


end
