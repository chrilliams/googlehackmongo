class Incident
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic

  index({location_array: "2dsphere"})

end
