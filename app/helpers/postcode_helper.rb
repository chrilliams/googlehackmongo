module PostcodeHelper

  def severity_lookup incident
    severity = case incident['accident_severity']
    when '1'
      'Fatal'
    when '2'
      'Serious'
    else
      'Minor'
    end
    severity
  end

  
  def title_for_incident incident
    severity = severity_lookup(incident)
    
    number_of_vehicles = incident['vehicles'].count
    "#{severity} accident with #{number_of_vehicles} #{'Vehicle'.pluralize(number_of_vehicles)}"
    
  end
  

  def daysofweek_lookup incident
    severity = case incident['day_of_week']
    when '1'
      'Sunday'
    when '2'
      'Monday'
    when '3'
      'Tuesday'
    when '4'
      'Wednesday'
    when '5'
      'Thursday'
    when '6'
      'Friday'
    else
      'Saturday'
    end
    severity
  end


  def gender_lookup vehicle 
    gender = case vehicle['sex_of_driver']
    when '1'
      'Male'
    when '2'
      'Female'
    when '3'
      'Not known'
    else
      'Data missing'
    end
  end

  def ageband_lookup vehicle
    agebank = case vehicle['age_band_of_driver']
    when '1'
      '0-5'
    when '2'
      '6-10'
    when '3'
      '11-15'
    when '4'
      '16-20'
    when '5'
      '21-25'
    when '6'
      '26-35'
    when '7'
      '36-45'
    when '8'
      '46-55'
    when '9'
      '56-65'
    when '10'
      '66-75'
    when '11'
      'Over 75'
    else
      'Data missing'
    end
  end
  
  def safety_average authority, incidents, average
    
    if incidents < average
      "#{authority.name} is safer than average"
    else
      "#{authority.name} is less safe than average"
    end 
  end
end
